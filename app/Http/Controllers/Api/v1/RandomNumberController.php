<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\RandomNumberResource;
use App\Models\RandomNumber;

class RandomNumberController extends Controller
{
    public function generate()
    {
        $randomNumber = new RandomNumber();
        $randomNumber->number = rand(1, 1000);
        $randomNumber->save();

        return new RandomNumberResource($randomNumber);
    }

    public function retrieve($id)
    {
        $randomNumber = RandomNumber::find($id);

        if ($randomNumber) {
            return new RandomNumberResource($randomNumber);
        } else {
            return response()->json(['error' => 'ID not found'], 404);
        }
    }
}
